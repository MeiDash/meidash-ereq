# MeiDash EReq 2021/2022

"Todo o entretenimento num só lugar"

- Diogo Filipe    uc2018288391@student.uc.pt  2018288391

- Henrique Fonseca    uc2021162362@student.uc.pt  2021162362

- José Francisco Bugalho  uc2021171129@student.uc.pt  2021171129

- José Gomes  uc2018286225@student.uc.pt  2018286225

- Pedro Marques   uc2018285632@student.uc.pt  2018285632

Link do MIRO: https://miro.com/app/board/o9J_lpTVn2M=/?invite_link_id=183005998256

Link FIGMA (Wireframes): https://www.figma.com/file/ZJlACVz7wiRQXZk6jgvrWk/Wireframes---EREQ?node-id=2%3A298
