{
  "actors": [
    {
      "id": "be7a2755-c74f-4c9c-aaa6-8a24f73cdabd",
      "text": "Utilizador de serviços de streaming",
      "type": "istar.Role",
      "x": 398,
      "y": 410,
      "customProperties": {
        "Description": ""
      },
      "nodes": []
    },
    {
      "id": "5e1ee087-25ba-4a22-9c6a-ab7df8028b47",
      "text": "MeiDash",
      "type": "istar.Agent",
      "x": 1000,
      "y": 543,
      "customProperties": {
        "Description": ""
      },
      "nodes": [
        {
          "id": "63b66c70-c21d-4d67-8fbc-1515b04e5608",
          "text": "Centraliza as plataformas e seus conteúdos",
          "type": "istar.Goal",
          "x": 1011,
          "y": 614,
          "customProperties": {
            "Description": ""
          }
        },
        {
          "id": "7b043e63-9b55-4b24-9751-dd6dfea4444d",
          "text": "Utilização aprimorada",
          "type": "istar.Quality",
          "x": 1034,
          "y": 708,
          "customProperties": {
            "Description": ""
          }
        },
        {
          "id": "b403f3dc-688c-44ec-875d-17da2ec7b5c1",
          "text": "Listar os favoritos",
          "type": "istar.Task",
          "x": 1000,
          "y": 833,
          "customProperties": {
            "Description": ""
          }
        },
        {
          "id": "6b7c3cb5-981a-476b-8cf5-ae84f0e51087",
          "text": "Catalogar os conteúdos",
          "type": "istar.Task",
          "x": 1101,
          "y": 813,
          "customProperties": {
            "Description": ""
          }
        },
        {
          "id": "273bcf13-12b8-4d76-923c-1e489dbe98f1",
          "text": "Apresentar métricas",
          "type": "istar.Task",
          "x": 1175,
          "y": 738,
          "customProperties": {
            "Description": ""
          }
        },
        {
          "id": "c13a4770-f478-4707-87c7-16fbb2033045",
          "text": "Segurança das contas assegurada",
          "type": "istar.Quality",
          "x": 1167,
          "y": 577,
          "customProperties": {
            "Description": ""
          }
        },
        {
          "id": "424d4116-2dcf-461f-b71f-385305b394c2",
          "text": "Conta única",
          "type": "istar.Quality",
          "x": 1282,
          "y": 676,
          "customProperties": {
            "Description": ""
          }
        },
        {
          "id": "44079236-db02-4b7a-a43a-aa8251275ad8",
          "text": "Encriptar credenciais",
          "type": "istar.Task",
          "x": 1320,
          "y": 608,
          "customProperties": {
            "Description": ""
          }
        }
      ]
    },
    {
      "id": "2045cce9-c8a6-45a7-b974-c6b94a526250",
      "text": "Utilizador",
      "type": "istar.Role",
      "x": 463,
      "y": 547,
      "customProperties": {
        "Description": ""
      },
      "nodes": [
        {
          "id": "0297ac33-d823-49ba-9d45-d8e2abe0a866",
          "text": "Ambiente organizado",
          "type": "istar.Goal",
          "x": 535,
          "y": 557,
          "customProperties": {
            "Description": ""
          }
        },
        {
          "id": "aff257b2-1495-4445-ac0d-2944a5b921cc",
          "text": "Acesso eficiente",
          "type": "istar.Quality",
          "x": 475,
          "y": 622,
          "customProperties": {
            "Description": ""
          }
        },
        {
          "id": "87820707-2475-452a-8155-ae90304af0ad",
          "text": "Computador",
          "type": "istar.Resource",
          "x": 493,
          "y": 703,
          "customProperties": {
            "Description": ""
          }
        },
        {
          "id": "98217ff9-b6f7-4ac2-a45e-7aca4535da5c",
          "text": "Subscrever plataforma de streaming",
          "type": "istar.Task",
          "x": 637,
          "y": 669,
          "customProperties": {
            "Description": ""
          }
        },
        {
          "id": "a386fd18-b803-4f9c-b577-53ea641b353c",
          "text": "Instalar plataforma de vídeojogos",
          "type": "istar.Task",
          "x": 674,
          "y": 580,
          "customProperties": {
            "Description": ""
          }
        }
      ]
    },
    {
      "id": "01520c45-5b8a-4c7b-90bd-2e1740192795",
      "text": "Plataforma",
      "type": "istar.Role",
      "x": 1057,
      "y": 328,
      "customProperties": {
        "Description": ""
      },
      "nodes": []
    },
    {
      "id": "dc4db3ba-15a4-4bd5-8874-867318f0d027",
      "text": "Plataforma de Vídeojogos",
      "type": "istar.Role",
      "x": 915,
      "y": 303,
      "customProperties": {
        "Description": ""
      },
      "nodes": []
    },
    {
      "id": "1124ba21-da1e-4196-b558-aa53e0d3d4f8",
      "text": "Plataforma de Streaming",
      "type": "istar.Role",
      "x": 1171,
      "y": 220,
      "customProperties": {
        "Description": ""
      },
      "nodes": []
    },
    {
      "id": "4d1fe055-6fd4-4e73-8125-d619a1644642",
      "text": "João Silva",
      "type": "istar.Agent",
      "x": 499,
      "y": 209,
      "customProperties": {
        "Description": ""
      },
      "nodes": []
    },
    {
      "id": "bebf00e2-0c23-468a-91bf-c58d81ee9208",
      "text": "Steam",
      "type": "istar.Agent",
      "x": 735,
      "y": 173,
      "customProperties": {
        "Description": ""
      },
      "nodes": []
    },
    {
      "id": "5e6c488b-8d69-4724-8e51-e3fbc0c67ced",
      "text": "Netflix",
      "type": "istar.Agent",
      "x": 983,
      "y": 161,
      "customProperties": {
        "Description": ""
      },
      "nodes": []
    },
    {
      "id": "a2785d6e-1f83-4163-9890-5061fac73b4d",
      "text": "Jogador de vídeojogos",
      "type": "istar.Role",
      "x": 570,
      "y": 402,
      "customProperties": {
        "Description": ""
      },
      "nodes": []
    }
  ],
  "orphans": [],
  "dependencies": [
    {
      "id": "b95f065c-2eb6-47f7-9a93-108e11c5b85e",
      "text": "Adicionar plataforma de vídeojogos",
      "type": "istar.Task",
      "x": 840,
      "y": 600,
      "customProperties": {
        "Description": ""
      },
      "source": "a386fd18-b803-4f9c-b577-53ea641b353c",
      "target": "63b66c70-c21d-4d67-8fbc-1515b04e5608"
    },
    {
      "id": "168cafa1-80ee-4df3-a6a5-93b1de7c8873",
      "text": "Adicionar plataforma de streaming",
      "type": "istar.Task",
      "x": 838,
      "y": 705,
      "customProperties": {
        "Description": ""
      },
      "source": "98217ff9-b6f7-4ac2-a45e-7aca4535da5c",
      "target": "63b66c70-c21d-4d67-8fbc-1515b04e5608"
    },
    {
      "id": "d2aad565-e2bb-4c2e-85c8-72bf9f7901e1",
      "text": "API's",
      "type": "istar.Resource",
      "x": 1032,
      "y": 441,
      "customProperties": {
        "Description": ""
      },
      "source": "63b66c70-c21d-4d67-8fbc-1515b04e5608",
      "target": "01520c45-5b8a-4c7b-90bd-2e1740192795"
    }
  ],
  "links": [
    {
      "id": "d8e914ff-60de-4050-83aa-3f8d949c9e5d",
      "type": "istar.DependencyLink",
      "source": "a386fd18-b803-4f9c-b577-53ea641b353c",
      "target": "b95f065c-2eb6-47f7-9a93-108e11c5b85e"
    },
    {
      "id": "e9523cdc-91fd-4ba3-8d75-e0d055e69c95",
      "type": "istar.DependencyLink",
      "source": "b95f065c-2eb6-47f7-9a93-108e11c5b85e",
      "target": "63b66c70-c21d-4d67-8fbc-1515b04e5608"
    },
    {
      "id": "ef86200b-92d4-4b5a-a379-738debd7c2b6",
      "type": "istar.DependencyLink",
      "source": "98217ff9-b6f7-4ac2-a45e-7aca4535da5c",
      "target": "168cafa1-80ee-4df3-a6a5-93b1de7c8873"
    },
    {
      "id": "00edb634-937f-454d-9141-ec733792d838",
      "type": "istar.DependencyLink",
      "source": "168cafa1-80ee-4df3-a6a5-93b1de7c8873",
      "target": "63b66c70-c21d-4d67-8fbc-1515b04e5608"
    },
    {
      "id": "d1727ed5-22e2-4ed2-895a-8569ebeeafa0",
      "type": "istar.DependencyLink",
      "source": "63b66c70-c21d-4d67-8fbc-1515b04e5608",
      "target": "d2aad565-e2bb-4c2e-85c8-72bf9f7901e1"
    },
    {
      "id": "5d7ba299-87a9-40e5-96c5-3eeff7304871",
      "type": "istar.DependencyLink",
      "source": "d2aad565-e2bb-4c2e-85c8-72bf9f7901e1",
      "target": "01520c45-5b8a-4c7b-90bd-2e1740192795"
    },
    {
      "id": "d9ace6be-ac33-4549-b4e3-2dd730de93de",
      "type": "istar.QualificationLink",
      "source": "aff257b2-1495-4445-ac0d-2944a5b921cc",
      "target": "0297ac33-d823-49ba-9d45-d8e2abe0a866"
    },
    {
      "id": "3cbe4fda-4d10-40d6-922a-5f5811d69e17",
      "type": "istar.OrRefinementLink",
      "source": "98217ff9-b6f7-4ac2-a45e-7aca4535da5c",
      "target": "0297ac33-d823-49ba-9d45-d8e2abe0a866"
    },
    {
      "id": "2640b218-e3b3-48d3-ade8-bce02f2ec648",
      "type": "istar.OrRefinementLink",
      "source": "a386fd18-b803-4f9c-b577-53ea641b353c",
      "target": "0297ac33-d823-49ba-9d45-d8e2abe0a866"
    },
    {
      "id": "72d6f156-d9aa-4390-a425-1aac53d3a817",
      "type": "istar.NeededByLink",
      "source": "87820707-2475-452a-8155-ae90304af0ad",
      "target": "a386fd18-b803-4f9c-b577-53ea641b353c"
    },
    {
      "id": "3b5a1360-5eeb-4ef1-9118-ff6dc93cac20",
      "type": "istar.NeededByLink",
      "source": "87820707-2475-452a-8155-ae90304af0ad",
      "target": "98217ff9-b6f7-4ac2-a45e-7aca4535da5c"
    },
    {
      "id": "8156ddf1-e89b-4104-904e-31b8060b3c46",
      "type": "istar.IsALink",
      "source": "dc4db3ba-15a4-4bd5-8874-867318f0d027",
      "target": "01520c45-5b8a-4c7b-90bd-2e1740192795"
    },
    {
      "id": "2f0d09fb-f548-4f38-9934-f7a5e35ac095",
      "type": "istar.IsALink",
      "source": "1124ba21-da1e-4196-b558-aa53e0d3d4f8",
      "target": "01520c45-5b8a-4c7b-90bd-2e1740192795"
    },
    {
      "id": "b2ec03d0-7f8a-41dc-a7c1-26a12ab63bac",
      "type": "istar.ParticipatesInLink",
      "source": "4d1fe055-6fd4-4e73-8125-d619a1644642",
      "target": "be7a2755-c74f-4c9c-aaa6-8a24f73cdabd"
    },
    {
      "id": "8c81bd2c-516e-4aa8-a7e2-97ffd95e22ff",
      "type": "istar.ParticipatesInLink",
      "source": "5e6c488b-8d69-4724-8e51-e3fbc0c67ced",
      "target": "1124ba21-da1e-4196-b558-aa53e0d3d4f8"
    },
    {
      "id": "dd3f313f-84fc-44b0-af7c-93d72a7026d9",
      "type": "istar.ParticipatesInLink",
      "source": "bebf00e2-0c23-468a-91bf-c58d81ee9208",
      "target": "dc4db3ba-15a4-4bd5-8874-867318f0d027"
    },
    {
      "id": "9982024b-ff6a-4103-8a1c-e5b2133b8275",
      "type": "istar.QualificationLink",
      "source": "7b043e63-9b55-4b24-9751-dd6dfea4444d",
      "target": "63b66c70-c21d-4d67-8fbc-1515b04e5608"
    },
    {
      "id": "6416007a-b8b4-418d-b843-b43c2b81c559",
      "type": "istar.ContributionLink",
      "source": "b403f3dc-688c-44ec-875d-17da2ec7b5c1",
      "target": "7b043e63-9b55-4b24-9751-dd6dfea4444d",
      "label": "help"
    },
    {
      "id": "ac11a904-4dd0-4977-8115-8718e2ecc696",
      "type": "istar.ContributionLink",
      "source": "6b7c3cb5-981a-476b-8cf5-ae84f0e51087",
      "target": "7b043e63-9b55-4b24-9751-dd6dfea4444d",
      "label": "help"
    },
    {
      "id": "05c5c12a-724d-473c-b76a-571f6b12e5c5",
      "type": "istar.ContributionLink",
      "source": "273bcf13-12b8-4d76-923c-1e489dbe98f1",
      "target": "7b043e63-9b55-4b24-9751-dd6dfea4444d",
      "label": "help"
    },
    {
      "id": "63476b0e-b6a3-4a69-a7f6-e40a7eaf8e2d",
      "type": "istar.QualificationLink",
      "source": "c13a4770-f478-4707-87c7-16fbb2033045",
      "target": "63b66c70-c21d-4d67-8fbc-1515b04e5608"
    },
    {
      "id": "73aff441-ba9c-4db9-a341-7f6c4d43a122",
      "type": "istar.ContributionLink",
      "source": "424d4116-2dcf-461f-b71f-385305b394c2",
      "target": "c13a4770-f478-4707-87c7-16fbb2033045",
      "label": "hurt"
    },
    {
      "id": "94f26b53-a705-4dbf-a4c1-a32c1ba4f24f",
      "type": "istar.ContributionLink",
      "source": "44079236-db02-4b7a-a43a-aa8251275ad8",
      "target": "c13a4770-f478-4707-87c7-16fbb2033045",
      "label": "make"
    },
    {
      "id": "0f237d8e-e523-44ed-ab27-c54370678d54",
      "type": "istar.IsALink",
      "source": "be7a2755-c74f-4c9c-aaa6-8a24f73cdabd",
      "target": "2045cce9-c8a6-45a7-b974-c6b94a526250"
    },
    {
      "id": "b2aac70f-0e24-4458-8312-9869aaf68ffe",
      "type": "istar.IsALink",
      "source": "a2785d6e-1f83-4163-9890-5061fac73b4d",
      "target": "2045cce9-c8a6-45a7-b974-c6b94a526250"
    },
    {
      "id": "48844b6f-3475-4ab1-a3bb-527b55aa58f1",
      "type": "istar.ParticipatesInLink",
      "source": "4d1fe055-6fd4-4e73-8125-d619a1644642",
      "target": "a2785d6e-1f83-4163-9890-5061fac73b4d"
    }
  ],
  "display": {
    "63b66c70-c21d-4d67-8fbc-1515b04e5608": {
      "width": 116.88748168945312,
      "height": 49.29999542236328
    },
    "d8e914ff-60de-4050-83aa-3f8d949c9e5d": {
      "vertices": [
        {
          "x": 755,
          "y": 601
        }
      ]
    },
    "ef86200b-92d4-4b5a-a379-738debd7c2b6": {
      "vertices": [
        {
          "x": 728,
          "y": 689
        }
      ]
    },
    "0f237d8e-e523-44ed-ab27-c54370678d54": {
      "vertices": [
        {
          "x": 438,
          "y": 472
        }
      ]
    },
    "be7a2755-c74f-4c9c-aaa6-8a24f73cdabd": {
      "collapsed": true
    },
    "01520c45-5b8a-4c7b-90bd-2e1740192795": {
      "collapsed": true
    },
    "dc4db3ba-15a4-4bd5-8874-867318f0d027": {
      "collapsed": true
    },
    "1124ba21-da1e-4196-b558-aa53e0d3d4f8": {
      "collapsed": true
    },
    "4d1fe055-6fd4-4e73-8125-d619a1644642": {
      "collapsed": true
    },
    "bebf00e2-0c23-468a-91bf-c58d81ee9208": {
      "collapsed": true
    },
    "5e6c488b-8d69-4724-8e51-e3fbc0c67ced": {
      "collapsed": true
    },
    "a2785d6e-1f83-4163-9890-5061fac73b4d": {
      "collapsed": true
    }
  },
  "tool": "pistar.2.0.0",
  "istar": "2.0",
  "saveDate": "Sat, 20 Nov 2021 15:15:54 GMT",
  "diagram": {
    "width": 2157,
    "height": 1300,
    "customProperties": {
      "Description": ""
    }
  }
}
